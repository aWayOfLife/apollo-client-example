import {gql} from '@apollo/client'

export const USERS_SUBSCRIPTION = gql`
  subscription newUser{
    newUser {
      name
    }
  }
`;