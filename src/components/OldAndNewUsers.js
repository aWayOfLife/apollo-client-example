import {useQuery, gql, useSubscription} from '@apollo/client'
import {LOAD_USERS} from '../graphql/Queries'
import {useEffect, useState} from 'react'
import {USERS_SUBSCRIPTION} from '../graphql/Subscriptions'

let unSubscribe = null

function OldAndNewUsers() {

    const {error, loading, data, subscribeToMore} = useQuery(LOAD_USERS)

    useEffect(()=>{
        subscribeToMore({
        document: USERS_SUBSCRIPTION,
        updateQuery: (prev, { subscriptionData }) => {
            if (!subscriptionData.data) return prev;
            const user = subscriptionData.data.newUser
            //return Object.assign({}, prev, { users: [...prev.users, user] })
            return {
                users: [...prev.users, user],
                
            };

        },
        });
        
    }, [])

    if (loading) return <p>"Loading...";</p>;
    if (error) return <p>`Error! ${error.message}`</p>;


    return (
       <div>
           {data.users.map((user) => (
        <div key={user._id}>
          <p>
            {user.name}
          </p>
        </div>
      ))}
       </div>

    )
}

export default OldAndNewUsers
