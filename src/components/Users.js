import {useQuery, gql} from '@apollo/client'
import {LOAD_USERS} from '../graphql/Queries'
import {useEffect, useState} from 'react'
function Users() {
    const [users,setUsers] = useState([])
    const {error, loading, data} = useQuery(LOAD_USERS)
    useEffect(()=>{
        if(data){
            console.log(data)
            setUsers(data)
        }
    }, [data])
    return (
       <div>
           {users && users.users && users.users.map((user,index)=>{
               return (
                   <h1 key={index}>{user.name}</h1>
               )
           })}
       </div>

    )
}

export default Users
