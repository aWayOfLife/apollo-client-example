import {useSubscription} from '@apollo/client'
import {USERS_SUBSCRIPTION} from '../graphql/Subscriptions'

function SubscribedUsers() {

    const { data , loading } = useSubscription(USERS_SUBSCRIPTION)
    if(data && data.newUser && data.newUser.name){
        console.log(data.newUser.name)
    }
    return (
        <div>
            {data && data.newUser && data.newUser.name ? <h1>{data.newUser.name}</h1>:null}
        </div>
    )
}

export default SubscribedUsers
