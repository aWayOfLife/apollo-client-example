import {CREATE_USER} from '../graphql/Mutatioins'
import {useMutation} from '@apollo/client'
import {useState} from 'react'
function AddUser() {
    const [name, setName] = useState("");
    const [age, setAge] = useState(0);
    const [createUser, {error}] = useMutation(CREATE_USER)
    const addUser = (e) =>{
        e.preventDefault()
        console.log(name, age)
        createUser({
            variables:{
                name:name,
                age:+age,
                married:false
            }
        })

        if(error){
            console.log(error)
        }
    }

    return (
        <form onSubmit={addUser}>
            <input
                type="text"
                placeholder="Name"
                value={name}
                onChange={(e) => {
                setName(e.target.value);
                }}
            />
            <input
                type="number"
                placeholder="Age"
                value={age}
                onChange={(e) => {
                setAge(e.target.value);
                }}
            />
            <button type="submit">Save</button>
        </form>
    )
}

export default AddUser
