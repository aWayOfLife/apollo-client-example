import './App.css';
import {ApolloClient, InMemoryCache, ApolloProvider, split, HttpLink} from '@apollo/client'
import {onError} from '@apollo/client/link/error'
import Users from './components/Users';
import AddUser from './components/AddUser';
import { WebSocketLink } from 'apollo-link-ws';
import {} from '@apollo/client';
import { getMainDefinition } from '@apollo/client/utilities';
import SubscribedUsers from './components/SubscribedUsers';
import OldAndNewUsers from './components/OldAndNewUsers';

const wsLink = new WebSocketLink({
  uri: `ws://localhost:5000/subscriptions`,
  options: {
    reconnect: true
  }
});


const link = split(
  // split based on operation type
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
    );
  },
  wsLink,
  new HttpLink({uri:'http://localhost:5000/graphql'})
);

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: link
})

function App() {
  return (
    <ApolloProvider client={client}>
      <div className="App">
        <OldAndNewUsers/>
      </div>
    </ApolloProvider>

  );
}

export default App;
